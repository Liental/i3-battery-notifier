last_capacity=$(($(exec cat /sys/class/power_supply/BAT1/capacity) + 1))

first_warning=100
second_warning=$((first_warning / 2))
third_warning=$((second_warning / 2))

f_done=false
s_done=false
t_done=false

function send_notification {
  ffplay -nodisp -autoexit -fast -sync audio -t 0.8 -volume 100 -alwaysontop ./insight.mp3 &> /dev/null &
  echo "+ Played the sound"

  sudo -u liental DISPLAY=:0 notify-send -u $1 "Battery Low - $2%" "Please charge your device." &
  echo "+ Sent the notification"
}

while true; do
  capacity=$(exec cat /sys/class/power_supply/BAT1/capacity)

  if [ "$capacity" -lt "$last_capacity" ]; then
    if [ "$f_done" == "false" ] && [ "$capacity" -le "$first_warning" ] && [ "$capacity" -gt "$second_warning" ]; then
      let f_done=true   
      send_notification "low" $capacity   
    fi
    
    if [ "$s_done" == "false" ] && [ "$capacity" -le "$second_warning" ] && [ "$capacity" -gt "$third_warning" ]; then
      let s_done=true      
      send_notification "normal" $capacity   
    fi

    if [ "$t_done" == "false" ] && [ "$capacity" -le "$third_warning" ]; then
      let t_done=true      
      send_notification "critical" $capacity   
    fi
  fi

  if [ "$capacity" -gt "$last_capacity" ]; then
    let f_done=false
    let s_done=false
    let t_done=false
  fi

  if [ "$capacity" != "$last_capacity" ]; then
    echo "Battery update: $capacity%"
  fi

  let last_capacity=$capacity
  sleep 5
done